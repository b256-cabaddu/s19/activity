let username, password, role;

function login() {
    username = prompt("Enter your username: ");
    password = prompt("Enter your password: ");
    role = prompt("Enter your role: ");
    if (username == "" || password == "" || role == "") {
        console.log("The input should NOT be EMPTY.")
    }
    else {
        switch (role) {
            case 'admin':
                alert('Welcome back to the portal, admin!');
                break;
            case 'teacher':
                alert('Thank you for loggin in, teacher!')
                break;
            case 'rookie':
                alert('Welcome to the class portal, student!')
                break;
            default:
                alert("Role out of range.")
                break;
        }
    }
}

login()

function checkAverage(a, b, c, d) {
    let average = Math.round((a + b + c + d) / 4)
    
    if(average <= 74) {
        console.log("Hello, student, your average is: " + average + ". " + "The letter equivalent is F.")
    }
    else if (average >= 75 && average < 80) {
        console.log("Hello, student, your average is: " + average + ". " + "The letter equivalent is D.")
    }
    else if (average >= 80 && average < 85) {
        console.log("Hello, student, your average is: " + average + ". " + "The letter equivalent is C.")
    }
    else if (average >= 85 && average <= 89) {
        console.log("Hello, student, your average is: " + average + ". " + "The letter equivalent is B.")
    }
    else if (average >= 90 && average <= 95) {
        console.log("Hello, student, your average is: " + average + ". " + "The letter equivalent is A.")
    }
    else if (average >= 96) {
        console.log("Hello, student, your average is: " + average + ". " + "The letter equivalent is A+.")
    }
}

/*  
    checkAverage(71, 70, 73, 74)
    checkAverage(75, 75, 76, 78)
    checkAverage(80, 81, 82, 78)
    checkAverage(84, 85, 87, 88)
    checkAverage(89, 90, 91, 90)
    checkAverage(91, 96, 97, 95)
*/

if (role === 'teacher' || role === 'admin' || role === undefined || role === "") {
    alert("<role>!" + " Your are not allowed to access this feature.")
}